<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/api', function(RouteBuilder $api) {
    $api->setExtensions('json');
    $api->connect('/system-updates',[
        'controller' => 'SystemUpdatesApi',
        'action' => 'getLastSystemUpdate'
    ]);
    $api->connect('/system-updates/avoid',[
        'controller' => 'SystemUpdatesApi',
        'action'=> 'avoidUpdates'
    ]); 
});

Router::connect('/', ['controller' => 'Home', 'action' => 'index']);

Router::scope('/cities', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Cities']);
    $routes->connect('/:action/*', ['controller' => 'Cities']);
});

Router::scope('/contacts', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Contacts']);
    $routes->connect('/:action/*', ['controller' => 'Contacts']);
});

Router::scope('/error', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Error']);
    $routes->connect('/:action/*', ['controller' => 'Error']);
});

Router::scope('/pages', function (RouteBuilder $routes) {
    $routes->connect('/*', ['controller' => 'Pages', 'action' => 'display']);
});

Router::scope('/reminders', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Reminders']);
    $routes->connect('/:action/*', ['controller' => 'Reminders']);
});

Router::scope('/segments', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Segments']);
    $routes->connect('/:action/*', ['controller' => 'Segments']);
});

Router::scope('/states', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'States']);
    $routes->connect('/:action/*', ['controller' => 'States']);
});

Router::scope('/system-updates', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'SystemUpdates']);
    $routes->connect('/:action/*', ['controller' => 'SystemUpdates']);
});

Router::scope('/users', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Users']);
    $routes->connect('/:action/*', ['controller' => 'Users']);
});


/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
