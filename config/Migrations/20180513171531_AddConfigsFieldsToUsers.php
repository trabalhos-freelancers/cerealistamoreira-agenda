<?php
use Migrations\AbstractMigration;

class AddConfigsFieldsToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('theme', 'string', [
            'default' => 'skin-red-light',
            'limit' => 255,
            'null' => true
        ]);
        $table->addColumn('font_size', 'integer', [
            'default' => 16,
            'limit' => 3,
            'null' => false,
        ]);
        $table->addColumn('last_system_update', 'integer',[
            'default' => 0,
            'limit' => 11,
            'null' => false,
        ]);
        $table->update();
    }
}
