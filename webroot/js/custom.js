$(document).ready(function() {
  // AdminLTE
  $(".navbar .menu").slimscroll({
      height: "200px",
      alwaysVisible: false,
      size: "3px"
  }).css("width", "100%");

  var a = $('a[href="' + baseUrl + requestUrl + '"]');
  if (!a.parent().hasClass('treeview')) {
      a.parent().addClass('active').parents('.treeview').addClass('active');
  }
  // AdminLTE

  // Select2
  $('.select2').select2();

  // Search cities
  function search_cities (options) {
    options.selected_by_text = options.selected_by_text || '';
    $.ajax({
      url: baseUrl + '/states/cities/' + options.state,
      dataType: 'json',
      beforeSend: function() {
          $('#city-id').attr('style', 'background: url('+baseUrl+'/img/loading.gif) no-repeat 95%;');
      },
      complete: function() {
          $('#city-id').attr('style', '');
      },
      success: function(data) {
        var html = '<option></option>';
        $.each(data.cities, function(i, item) {
          var selected = '';
          if (options.selected_by_text != '' && options.selected_by_text == item.name) {
            selected = ' selected="selected"';
          }
          html = html + '<option value="'+item.id+'"'+selected+'>' + item.name + '</option>';
        });
        $('#city-id').html(html);
      }
    });
  };

  $('#state-id').change(function() {
    var state = $(this);
    search_cities({
      state: $(this).val()
    });
  });

  // CEP
  $('.search_by_cep').change(function() {
    var cepf = $(this);
    var cep = cepf.val();
    if (cep.length == 8) {
      $.ajax({
          url: 'https://viacep.com.br/ws/'+cep+'/json/',
          dataType: 'json',
          beforeSend: function() {
              cepf.attr('style', 'background: url('+baseUrl+'/img/loading.gif) no-repeat 95%;');
          },
          complete: function() {
              cepf.attr('style', '');
          },
          success: function(data) {
            if (data.erro) {
              alert('CEP não foi encontrado.');
            } else {
              $('input#address').val(data.logradouro);
              $('input#address-complement').val(data.complemento);
              $('input#address-neighborhood').val(data.bairro);
              $('input#address-city').val(data.localidade);
              $('select#address-state').val(data.uf);
              var state_selected = $("select#state-id option:contains('"+data.uf+"')");
              var state_id = state_selected.val();
              state_selected.attr('selected', true);
              search_cities({
                state: state_id,
                selected_by_text: data.localidade
              });
            }
          }
      });
    } else {
      alert('Informe um cep válido.');
    }
  });
});
