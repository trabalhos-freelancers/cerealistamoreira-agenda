<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.7
    </div>
    <strong>Copyright &copy; <?= date('Y'); ?> <a href="#" target="_blank">CM</a>.</strong> Todos os direitos reservados.
</footer>
