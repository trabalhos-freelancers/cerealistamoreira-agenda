<!-- Modal -->
<div class="modal fade" id="modal-new-system-update" tabindex="-1" role="dialog" aria-labelledby="modal-new-system-updateLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-new-system-updateLabel">Nova Atualização no Sistema</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <a type="button" class="btn btn-primary" href="/system-updates">Ver todas as Atualizações</a>
            </div>
        </div>
    </div>
</div>
