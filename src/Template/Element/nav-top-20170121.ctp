<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <?php
            if ($current_user):
            ?>
            <li>
                <a href="#">
                    <i class="fa fa-user"></i> <?= h($current_user['username']) ?></span>
                </a>
            </li>
            <li>
                <a href="<?php echo $this->Url->build('/users/customize'); ?>">
                    <i class="fa fa-gears"></i> <?= __('Customize') ?></span>
                </a>
            </li>
            <li>
                <a href="<?php echo $this->Url->build('/users/logout'); ?>">
                    <i class="fa fa-sign-out"></i> <?= __('Logout') ?></span>
                </a>
            </li>
            <?php endif; ?>
            <!-- Control Sidebar Toggle Button -->
        </ul>
    </div>
</nav>
