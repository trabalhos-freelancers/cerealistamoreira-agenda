<?php
use Cake\ORM\TableRegistry;
$SystemUpdates = TableRegistry::get('SystemUpdates');
?>
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <?php if ($current_user): ?>
            <li>
                <a href="<?php echo $this->Url->build('/system-updates'); ?>" title="<?php echo __('System Updates'); ?>" alt="<?php echo __('System Updates'); ?>">
                    <i class="fa fa-bell-o"></i>
                    <?php
                        $total_dont_read = $SystemUpdates->totalDontRead( $current_user['last_system_update'] );
                        if ($total_dont_read > 0):
                    ?>
                        <span class="label label-warning"><?php echo h( $total_dont_read ); ?></span>
                    <?php endif; ?>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-user"></i> <span><?= h($current_user['username']) ?></span>
                </a>
            </li>
            <li>
                <a href="<?php echo $this->Url->build('/users/customize'); ?>">
                    <i class="fa fa-gears"></i> <span class="hidden-xs"><?= __('Customize') ?></span>
                </a>
            </li>
            <li>
                <a href="<?php echo $this->Url->build('/users/logout'); ?>">
                    <i class="fa fa-sign-out"></i> <span class="hidden-xs"><?= __('Logout') ?></span>
                </a>
            </li>
            <?php endif; ?>
            <!-- Control Sidebar Toggle Button -->
        </ul>
    </div>
</nav>
