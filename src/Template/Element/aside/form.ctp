<?php
echo $this->Form->create(null, [
  'url' => '/contacts',
  'type' => 'get',
  'class' => 'sidebar-form'
]);
?>
<div class="input-group">
    <?php
    echo $this->Form->input('q', [
        'label' => false,
        'class' => 'form-control',
        'placeholder' => __('Search contact...')
    ]);
    ?>
    <span class="input-group-btn">
        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
        </button>
    </span>
</div>
<?= $this->Form->end(); ?>
