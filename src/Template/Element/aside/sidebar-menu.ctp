<ul class="sidebar-menu">
    <li class="header"><?= __('MENU PRINCIPAL') ?></li>
    <li>
        <a href="<?php echo $this->Url->build('/'); ?>">
            <i class="fa fa-dashboard"></i> <span><?= __('Dashboard') ?></span>
        </a>
    </li>
    <li>
        <a href="<?php echo $this->Url->build('/contacts'); ?>">
            <i class="fa fa-users"></i> <span><?= __('Contacts') ?></span>
        </a>
    </li>
    <li>
        <a href="<?php echo $this->Url->build('/reminders'); ?>">
            <i class="fa fa-sticky-note"></i> <span><?= __('Reminders') ?></span>
        </a>
    </li>

    <li class="treeview">
      <a href="#">
        <i class="fa fa-cog"></i> <span><?= __('Settings') ?></span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu" style="display: none;">
        <li>
            <a href="<?php echo $this->Url->build('/users/customize'); ?>">
                <i class="fa fa-gears"></i> <?= __('Customize') ?>
            </a>
        </li>
        <li>
            <a href="<?php echo $this->Url->build('/system-updates'); ?>">
                <i class="fa fa-refresh"></i><?= __('System Updates') ?>
            </a>
        </li>
        <?php if ($current_user['role'] == 'admin'): ?>
        <li>
            <a href="<?php echo $this->Url->build('/segments'); ?>">
                <i class="fa fa-list"></i> <?= __('Segments') ?>
            </a>
        </li>
        <li>
            <a href="<?php echo $this->Url->build('/users'); ?>">
                <i class="fa fa-user"></i><?= __('Users') ?>
            </a>
        </li>
        <li>
            <a href="<?php echo $this->Url->build('/cities'); ?>">
                <i class="fa fa-circle-o"></i><?= __('Cities') ?>
            </a>
        </li>
        <li>
            <a href="<?php echo $this->Url->build('/states'); ?>">
                <i class="fa fa-circle-o"></i><?= __('States') ?>
            </a>
        </li>
        <?php endif; ?>
      </ul>
    </li>

    <?php if (Cake\Core\Configure::read('debug')) { ?>
    <li><a href="<?php echo $this->Url->build('/pages/debug'); ?>"><i class="fa fa-bug"></i> Debug</a></li>
    <?php } ?>
</ul>
