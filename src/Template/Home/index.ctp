<?php
use Cake\I18n\Time;
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= __('Dashboard') ?>
    <small><?= __('Control panel') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><i class="fa fa-dashboard"></i> <?= __('Dashboard') ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

  <div class="box text-center">
    <div class="box-body">
      <h1>Cerealista Moreira</h1>
      <?php
      $time = Time::now();
      $hour = $time->hour;
      $txt = '';
      if ($hour > 6 && $hour < 12) {
        $txt = 'Bom dia';
      } elseif ($hour > 12 && $hour < 18) {
        $txt = 'Boa tarde';
      } else {
        $txt = 'Boa noite';
      }
      ?>
      <h3><?php echo $txt; ?> <?= h($current_user['name_or_username']) ?>, <?= $time->i18nFormat([\IntlDateFormatter::FULL, \IntlDateFormatter::SHORT]); ?></h3>
    </div>
  </div>

  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

        <div class="info-box-content">
          <span class="info-box-text"><?= __('Contacts') ?></span>
          <span class="info-box-number"><?= $this->Html->link($this->Number->format($totalContacts), ['controller' => 'Contacts']); ?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-sticky-note"></i></span>

        <div class="info-box-content">
          <span class="info-box-text"> <?= __('Reminders') ?></span>
          <span class="info-box-number"><?= $this->Html->link($this->Number->format($reminderTotalOpen), ['controller' => 'Reminders']); ?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-birthday-cake"></i></span>

        <div class="info-box-content">
          <span class="info-box-text"><?= __('Birthdays') ?></span>
          <span class="info-box-number"><?= $this->Number->format($birthdays->count()); ?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-list"></i></span>

        <div class="info-box-content">
          <span class="info-box-text"><?= __('Segments') ?></span>
          <span class="info-box-number"><?= $this->Html->link($this->Number->format($totalSegments), ['controller' => 'Segments']); ?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>

  <div class="row">
    <div class="col-lg-6">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-birthday-cake"></i> <?= __('Birthdays') ?></h3>
        </div>
        <div class="box-body">
          <div class="box-group" id="accordion">
              <?php foreach ($birthdays as $contact): ?>
              <div class="panel box box-default">
                  <div class="box-header with-border">
                      <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= h($contact->id) ?>" aria-expanded="true" class="">
                          <?= h($contact->born_date->i18nFormat('dd/MM')); ?> - <?= h($contact->name) ?>
                        </a>
                      </h4>
                      <?php if($current_user['role'] == 'admin'): ?>
                      <div class="box-tools">
                          <?= $this->Html->link(__('Edit'), ['controller' => 'contacts', 'action' => 'edit', $contact->id]) ?>
                      </div>
                      <?php endif; ?>
                  </div>
                  <div id="collapse<?= h($contact->id) ?>" class="panel-collapse collapse" aria-expanded="true">
                      <div class="box-body box-contact">
                        <ul class="list-unstyled list-contact-info">
                          <?php if ($contact->contact_type): ?>
                          <li>
                              <strong><?= __('Contact Type') ?>:</strong>
                              <?= $contact->contact_type ?>
                          </li>
                          <?php endif; ?>

                          <?php if ($contact->has('segment')): ?>
                          <li>
                              <strong><?= __('Segment') ?>:</strong>
                              <?= $contact->segment->name ?>
                          </li>
                          <?php endif; ?>

                          <?php if ($contact->contact_name): ?>
                          <li>
                              <strong><?= __('Contact') ?>:</strong>
                              <?= $contact->contact_name ?>
                          </li>
                          <?php endif; ?>

                          <?php if ($contact->department): ?>
                          <li>
                              <strong><?= __('Department') ?>:</strong>
                              <?= $contact->department ?>
                          </li>
                          <?php endif; ?>

                          <?php if ($contact->telephones): ?>
                          <li>
                              <strong><?= __('Telephone') ?>:</strong>
                              <?= $contact->telephones ?>
                          </li>
                          <?php endif; ?>

                          <?php if ($contact->fax): ?>
                          <li>
                              <strong><?= __('Fax') ?>:</strong>
                              <?= $contact->fax ?>
                          </li>
                          <?php endif; ?>

                          <?php if ($contact->mobile): ?>
                          <li>
                              <strong><?= __('Mobile') ?>:</strong>
                              <?= $contact->mobile ?>
                          </li>
                          <?php endif; ?>

                          <?php if ($contact->email): ?>
                          <li>
                              <strong><?= __('E-mail') ?>:</strong>
                              <?= $contact->email ?>
                          </li>
                          <?php endif; ?>

                          <?php if ($contact->skype): ?>
                          <li>
                              <strong><?= __('Skype') ?>:</strong>
                              <?= $contact->skype ?>
                          </li>
                          <?php endif; ?>

                          <?php if ($contact->site): ?>
                          <li>
                              <strong><?= __('Site') ?>:</strong>
                              <?= $contact->site ?>
                          </li>
                          <?php endif; ?>

                          <?php if ($contact->born_date): ?>
                          <li>
                              <strong><?= __('Born date') ?>:</strong>
                              <?= $contact->born_date ?>
                          </li>
                          <?php endif; ?>
                          </ul>

                            <ul class="list-unstyled list-contact-info">
                                <li><strong><i class="fa fa-map"></i> <?= __('Address') ?></strong><br /></li>
                                <?php if ($contact->address): ?>
                                <li><?= __('Address') ?>: <?= h($contact->address) ?></li>
                                <?php endif;?>

                                <?php if ($contact->address_number): ?>
                                <li><?= __('Address Number') ?>: <?= h($contact->address_number) ?></li>
                                <?php endif;?>

                                <?php if ($contact->address_complement): ?>
                                <li><?= __('Address Complement') ?>: <?= h($contact->address_complement) ?></li>
                                <?php endif;?>

                                <?php if ($contact->address_neighborhood): ?>
                                <li><?= __('Address Neighborhood') ?>: <?= h($contact->address_neighborhood) ?></li>
                                <?php endif;?>

                                <?php /*if ($contact->address_city): ?>
                                <li><?= __('Address City') ?>: <?= h($contact->address_city) ?></li>
                                <?php endif;?>

                                <?php if ($contact->address_state): ?>
                                <li><?= __('Address State') ?>: <?= h($contact->address_state) ?></li>
                                <?php endif;*/ ?>

                                <?php if ($contact->has('city')): ?>
                                <li>
                                    <?= __('Address City') ?>: <?= $contact->city->name ?>
                                </li>
                                <?php endif; ?>

                                <?php if ($contact->has('state')): ?>
                                <li>
                                    <?= __('Address State') ?>: <?= $contact->state->name ?>
                                </li>
                                <?php endif; ?>

                                <?php if ($contact->address_zipcode): ?>
                                <li><?= __('Address Zipcode') ?>: <?= h($contact->address_zipcode) ?></li>
                                <?php endif;?>
                            </ul>

                          <?php if ($contact->note): ?>
                          <p>
                              <strong><i class="fa fa-sticky-note"></i> <?= __('Note') ?>:</strong><br />
                              <?php echo h($contact->note); ?>
                          </p>
                          <?php endif; ?>
                      </div>
                  </div>
              </div>
              <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-sticky-note"></i> Tarefas nos próximos sete dias</h3>
        </div>
        <table class="table">
          <tbody>
            <?php foreach ($reminders as $reminder): ?>
            <tr>
                <td><?= h($reminder->due_date) ?></td>
                <td><?= h(strip_tags($reminder->content)) ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title"><?= __('Top Segments') ?></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-8">
          <div class="chart-responsive">
            <canvas id="pieChart" height="150"></canvas>
          </div>
          <!-- ./chart-responsive -->
        </div>
        <!-- /.col -->
        <div class="col-md-4">
          <ul class="list-group">
            <?php foreach($top10Segments as $segment): ?>
            <li class="list-group-item">
              <?php echo $this->Html->link(h($segment['name']), ['controller' => 'contacts', 'action' => 'index', 'segment_id' => $segment['id']]); ?>
              <span class="badge pull-right"><?= h($segment['total_contacts']) ?></span>
            </li>
            <?php endforeach; ?>
          </ul>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.box-body -->
    <!-- /.footer -->
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->

<?= $this->element('new-system-update') ?>

<?php
function rand_color() {
    return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
}
?>
<?php echo $this->Html->script('/plugins/chartjs/Chart.min'); ?>
<?php $this->Html->scriptStart(['block' => true]); ?>
var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
var pieChart = new Chart(pieChartCanvas);
var PieData = [
<?php
foreach($top10Segments as $segment):
  $color = rand_color();
?>
  {
    value: <?= h($segment['total_contacts']); ?>,
    label: "<?= h($segment['name']); ?>",
    color: "<?= $color; ?>",
    highlight: "<?= $color; ?>"
  },
<?php endforeach; ?>
];

var pieOptions = {
  segmentShowStroke: true,
  segmentStrokeColor: "#fff",
  segmentStrokeWidth: 1,
  percentageInnerCutout: 50,
  animationSteps: 100,
  animationEasing: "easeOutBounce",
  animateRotate: true,
  animateScale: false,
  responsive: true,
  maintainAspectRatio: true,
  legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
  tooltipTemplate: "<%=value %> <%=label%> users"
};
pieChart.Doughnut(PieData, pieOptions);
<?php $this->Html->scriptEnd(); ?>

<?php $this->start('scriptBotton') ?>
<script>
  $.get('/api/system-updates.json')
   .done(function( response ) {
     if ( response != null ) {
       /** populando modal */
       var modal = $('#modal-new-system-update .modal-body');
       var html = '<div class="box box-solid">'
           html += '<div class="box-body">'
           html += '<dl>'
           switch (response[0].update_type) {
             case 'bug':
               html += '<dt><h4 class="bg-danger"><i class="fa fa-bug fa-fw"></i>'+ response[0].name +'</h4></dt>';
               break;
           
             case 'new_feature':
               html += '<dt><h4 class="bg-warning"><i class="fa fa-star fa-fw"></i>'+ response[0].name +'</h4></dt>'
               break;
           }
           html += '<dd>'+response[0].description+'</dd>'
           html += '</dl>'
           html += '</div>'
           html += '</div>'
     }

     modal.html( html );
     $('#modal-new-system-update').modal('show');
   })
   .fail();

   $('#modal-new-system-update').on('hidden.bs.modal', function () {
        $.get('/api/system-updates/avoid.json');
    })
</script>
<?php $this->end() ?>
