<section class="content-header">
    <h1><i class="fa fa-refresh"></i> <?= __('System Updates') ?><small><?= __('Edit') ?></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-dashboard"></i> <?= __('Dashboard') ?></a></li>
        <li><?= $this->Html->link(__('System Updates'), ['action' => 'index']) ?></li>
        <li class="active"><?= __('Edit') ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default with-border">
        <div class="box-body">
            <?= $this->Form->create($systemUpdate) ?>
                <div class="row">
                    <div class="col-md-4">
                        <?php echo $this->Form->input('update_date'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php
                        echo $this->Form->input('update_type', [
                            'options' => \App\Model\Table\SystemUpdatesTable::updateTypeOptions()
                        ]);
                        ?>
                    </div>
                </div>
                <?php
                    echo $this->Form->input('name');
                    echo $this->Form->input('description');
                ?>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
        <div class="box-footer">
            <ul class="list-inline">
                <li>
                <?= $this->Form->postLink(
                        __('Delete'),
                        ['action' => 'delete', $systemUpdate->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $systemUpdate->id)]
                    )
                ?>
                </li>
                <li><?= $this->Html->link(__('Back list'), ['action' => 'index']) ?></li>
            </ul>
        </div>
    </div>
</section>

<?php $this->start('scriptBotton') ?>
    <!-- include summernote css/js -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#description').summernote();
        });
    </script>
<?php $this->end() ?>

