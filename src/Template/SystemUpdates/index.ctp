<section class="content-header">
    <h1><i class="fa fa-refresh"></i> <?= __('System Updates') ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-dashboard"></i> <?= __('Dashboard') ?></a></li>
        <li class="active"><?= __('System Updates') ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default">
        <?php if ($current_user['role'] == 'admin'): ?>
        <div class="box-header with-border">
            <?= $this->Html->link(__('New System Update'), ['action' => 'add'], ['class' => 'btn btn-sm btn-success']) ?>
        </div>
        <?php endif; ?>
        <div class="box-body">
            <?php if ($systemUpdates->count() > 0): ?>
            <ul class="timeline timeline-inverse">
                <?php
                $current_date = NULL;
                foreach ($systemUpdates as $systemUpdate):
                    if ($current_date == NULL or $current_date != $systemUpdate->update_date):
                    ?>
                        <li class="time-label">
                            <span class="bg-red">
                                <?php echo h( $systemUpdate->update_date->i18nFormat('dd MMM yyyy') ); ?>
                            </span>
                        </li>
                    <?php
                        $current_date = $systemUpdate->update_date;
                    endif;

                    switch ($systemUpdate->update_type):
                        case 'bug':
                            $icon = 'bug';
                            $icon_bg = 'bg-black';
                            break;

                        case 'new_feature':
                            $icon = 'star';
                            $icon_bg = 'bg-yellow';
                            break;
                    endswitch;
                ?>
                    <li>
                        <i class="fa fa-<?php echo $icon; ?> <?php echo $icon_bg; ?>"></i>
                        <div class="timeline-item">
                            <h3 class="timeline-header"><?php echo h( $systemUpdate->name ); ?></h3>
                            <?php if ($systemUpdate->description != ''): ?>
                            <div class="timeline-body">
                                <?= $systemUpdate->description ?>
                            </div>
                            <?php endif; ?>
                            <?php if ($current_user['role'] == 'admin'): ?>
                            <div class="timeline-footer">
                                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $systemUpdate->id], ['class' => 'btn btn-primary btn-sm']) ?>
                                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $systemUpdate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $systemUpdate->id), 'class' => 'btn btn-danger btn-sm']) ?>
                            </div>
                            <?php endif; ?>
                        </div>
                    </li>
                <?php endforeach; ?>
                <li>
                    <i class="fa fa-clock-o bg-gray"></i>
                </li>
            </ul>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <li><a href="#"><?= $this->Paginator->counter(__('{{count}} records. Page {{page}} of {{pages}}')); ?></a></li>
                </ul>
            </div>
            <?php else: ?>
            <p><?= __('No records found') ?></p>
            <?php endif; ?>
        </div>
    </div>
</section>
