<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit System Update'), ['action' => 'edit', $systemUpdate->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete System Update'), ['action' => 'delete', $systemUpdate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $systemUpdate->id)]) ?> </li>
        <li><?= $this->Html->link(__('List System Updates'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New System Update'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="systemUpdates view large-9 medium-8 columns content">
    <h3><?= h($systemUpdate->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Update Type') ?></th>
            <td><?= h($systemUpdate->update_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($systemUpdate->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($systemUpdate->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Update Date') ?></th>
            <td><?= h($systemUpdate->update_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($systemUpdate->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($systemUpdate->updated) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($systemUpdate->description)); ?>
    </div>
</div>
