<section class="content-header">
    <h1><i class="fa fa-sticky-note"></i> <?= __('Reminders') ?><small><?= __('Edit') ?></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-dashboard"></i> <?= __('Dashboard') ?></a></li>
        <li><?= $this->Html->link(__('Reminders'), ['action' => 'index']) ?></li>
        <li class="active"><?= __('Edit') ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default with-border">
        <div class="box-body">
            <?= $this->Form->create($reminder) ?>
                <?php
                echo $this->Form->input('due_date', [
                    'monthNames' => true,
                    'day' => ['class' => 'form-control'],
                    'month' => ['names' => true],
                    'year' => [
                        'start' => date('Y'),
                        'end' => date('Y') + 20,
                        'order' => 'ASC'
                    ]
                ]);
                ?>
                <?php echo $this->Form->input('content', ['id' => 'reminder-content']); ?>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                        <div class="form-group">
                            <label for="author">Autor</label>
                            <input 
                                id="author"
                                name="author" 
                                class="form-control"
                                value="<?= $current_user['name']?>"
                                readonly
                            >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        <div class="form-group">
                            <label for="visibility">Visibilidade</label>
                            <select class="form-control" name="visibility">
                                <option value="PUBLIC" <?php if($reminder->visibility === 'PUBLIC') echo 'selected'?>>Público</option>
                                <option value="PRIVATE" <?php if($reminder->visibility === 'PRIVATE') echo 'selected'?>>Privado</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                        <div class="form-group">
                            <label for="user_id">Usuário Associado</label>
                            <select id="user_id" class="form-control" name="user_id">
                                <option>...</option>
                                <?php foreach ($users as $user): ?>
                                    <?php if( !($current_user['id'] === $user->id) ): ?>
                                        <option value="<?= $user->id ?>" <?php if( $user->id === $reminder->user_id) echo 'selected'?> ><?= $user->name ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
        <div class="box-footer">
            <ul class="list-inline">
                <li>
                <?= $this->Form->postLink(
                        __('Delete'),
                        ['action' => 'delete', $reminder->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $reminder->id)]
                    )
                ?>
                </li>
                <li><?= $this->Html->link(__('Back list'), ['action' => 'index']) ?></li>
            </ul>
        </div>
    </div>
</section>

<?php $this->start('scriptBotton') ?>
    <!-- include summernote css/js -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#reminder-content').summernote();
        });
    </script>
<?php $this->end() ?>
