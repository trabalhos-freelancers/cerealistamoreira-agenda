<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Reminder'), ['action' => 'edit', $reminder->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Reminder'), ['action' => 'delete', $reminder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reminder->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Reminders'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Reminder'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="reminders view large-9 medium-8 columns content">
    <h3><?= h($reminder->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($reminder->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Due Date') ?></th>
            <td><?= h($reminder->due_date) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Content') ?></h4>
        <?= $this->Text->autoParagraph(h($reminder->content)); ?>
    </div>
</div>
