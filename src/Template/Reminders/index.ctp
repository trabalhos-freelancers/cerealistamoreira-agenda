<section class="content-header">
    <h1><i class="fa fa-sticky-note"></i> <?= __('Reminders') ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-dashboard"></i> <?= __('Dashboard') ?></a></li>
        <li class="active"><?= __('Reminders') ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <?= $this->Html->link(__('New Reminder'), ['action' => 'add'], ['class' => 'btn btn-sm btn-success']) ?>
            <div class="box-tools">
                <?= $this->Form->create(null, [
                    'url' => '/reminders',
                    'type' => 'get'
                ]); ?>
                <div class="input-group input-group-sm" style="width: 150px;">
                    <?php
                    echo $this->Form->input('q', [
                        'label' => false,
                        'class' => 'form-control input-sm pull-right',
                        'placeholder' => __('Search'),
                        'value' => h($this->request->getQuery('q'))
                    ]); ?>
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
        <div class="box-body">
        <?php if ($reminders->count() > 0): ?>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('due_date') ?></th>
                        <th scope="col"><?= __('content') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($reminders as $reminder): ?>
                    <tr>
                        <td><?= h($reminder->due_date) ?></td>
                        <td><?= strip_tags($reminder->content) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $reminder->id]) ?>
                            |
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $reminder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reminder->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <li><a href="#"><?= $this->Paginator->counter(__('{{count}} records. Page {{page}} of {{pages}}')); ?></a></li>
                </ul>
            </div>
        <?php else: ?>
            <p><?= __('No records found') ?></p>
        <?php endif; ?>
        </div>
    </div>
</section>
