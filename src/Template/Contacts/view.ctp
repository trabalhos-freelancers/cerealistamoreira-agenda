<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Contact'), ['action' => 'edit', $contact->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Contact'), ['action' => 'delete', $contact->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contact->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Contacts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contact'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Segments'), ['controller' => 'Segments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Segment'), ['controller' => 'Segments', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="contacts view large-9 medium-8 columns content">
    <h3><?= h($contact->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($contact->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contact Name') ?></th>
            <td><?= h($contact->contact_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Department') ?></th>
            <td><?= h($contact->department) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Segment') ?></th>
            <td><?= $contact->has('segment') ? $this->Html->link($contact->segment->name, ['controller' => 'Segments', 'action' => 'view', $contact->segment->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contact Type') ?></th>
            <td><?= h($contact->contact_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($contact->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address Number') ?></th>
            <td><?= h($contact->address_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address Complement') ?></th>
            <td><?= h($contact->address_complement) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address Neighborhood') ?></th>
            <td><?= h($contact->address_neighborhood) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address City') ?></th>
            <td><?= h($contact->address_city) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address State') ?></th>
            <td><?= h($contact->address_state) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address Zipcode') ?></th>
            <td><?= h($contact->address_zipcode) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Telephones') ?></th>
            <td><?= h($contact->telephones) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fax') ?></th>
            <td><?= h($contact->fax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mobile') ?></th>
            <td><?= h($contact->mobile) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($contact->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Skype') ?></th>
            <td><?= h($contact->skype) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Site') ?></th>
            <td><?= h($contact->site) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($contact->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Born Date') ?></th>
            <td><?= h($contact->born_date) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Note') ?></h4>
        <?= $this->Text->autoParagraph(h($contact->note)); ?>
    </div>
</div>
