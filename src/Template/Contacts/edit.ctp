<section class="content-header">
    <h1><i class="fa fa-users"></i> <?= __('Contacts') ?><small><?= __('Edit') ?></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-dashboard"></i> <?= __('Dashboard') ?></a></li>
        <li><?= $this->Html->link(__('Contacts'), ['action' => 'index']) ?></li>
        <li class="active"><?= __('Edit') ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default with-border">
        <div class="box-body">
            <?= $this->Form->create($contact) ?>
                <?php echo $this->Form->input('name'); ?>
                <div class="row">
                    <div class="col-md-4"><?php echo $this->Form->input('segment_id', ['options' => $segments, 'class' => 'select2', 'empty' => true]); ?></div>
                    <div class="col-md-4"><?php echo $this->Form->input('contact_type', ['options' => $contactTypesOptions]); ?></div>
                    <div class="col-md-4">
                        <?php
                        echo $this->Form->input('born_date', [
                            'monthNames' => true,
                            'day' => ['class' => 'form-control'],
                            'month' => ['names' => true],
                            'year' => [
                                'start' => date('Y') - 80,
                                'end' => date('Y') - 16
                            ],
                            'empty' => true
                        ]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6"><?php echo $this->Form->input('contact_name'); ?></div>
                    <div class="col-md-6"><?php echo $this->Form->input('department'); ?></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><?php echo $this->Form->input('telephones'); ?></div>
                    <div class="col-md-4"><?php echo $this->Form->input('fax'); ?></div>
                    <div class="col-md-4"><?php echo $this->Form->input('mobile'); ?></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><?php echo $this->Form->input('email'); ?></div>
                    <div class="col-md-4"><?php echo $this->Form->input('skype'); ?></div>
                    <div class="col-md-4"><?php echo $this->Form->input('site'); ?></div>
                </div>
                <div class="row">
                    <div class="col-md-3"><?php echo $this->Form->input('address_zipcode', ['class' => 'search_by_cep', 'maxlength' => '8']); ?></div>
                </div>
                <div class="row">
                    <div class="col-md-10"><?php echo $this->Form->input('address'); ?></div>
                    <div class="col-md-2"><?php echo $this->Form->input('address_number'); ?></div>
                </div>
                <?php echo $this->Form->input('address_complement'); ?>
                <div class="row">
                    <div class="col-md-3"><?php echo $this->Form->input('address_neighborhood'); ?></div>
                    <?php if(false): ?>
                    <div class="col-md-3"><?php echo $this->Form->input('address_city'); ?></div>
                    <div class="col-md-3"><?php echo $this->Form->input('address_state', ['options' => $stateOptions, 'empty' => true]); ?></div>
                    <?php endif; ?>
                    <div class="col-md-3"><?php echo $this->Form->input('state_id', ['options' => $states, 'empty' => true]); ?></div>
                    <div class="col-md-3"><?php echo $this->Form->input('city_id'); ?></div>
                </div>
                <?php echo $this->Form->input('note'); ?>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
        <div class="box-footer">
            <ul class="list-inline">
                <li>
                <?= $this->Form->postLink(
                        __('Delete'),
                        ['action' => 'delete', $contact->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $contact->id)]
                    )
                ?>
                </li>
                <li><?= $this->Html->link(__('Back list'), ['action' => 'index']) ?></li>
            </ul>
        </div>
    </div>
</section>
