<section class="content-header">
    <h1><i class="fa fa-users"></i> <?= __('Contacts') ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-dashboard"></i> <?= __('Dashboard') ?></a></li>
        <li class="active"><?= __('Contacts') ?></li>
    </ol>
</section>

<section class="content contacts">
    <div class="box box-default">
        <div class="box-header with-border">
            <?= $this->Html->link(__('New Contact'), ['action' => 'add'], ['class' => 'btn btn-sm btn-success']) ?>
            <div class="box-tools">
                <?= $this->Form->create(null, [
                    'url' => '/contacts',
                    'type' => 'get',
                    'class' => 'form-inline search'
                ]); ?>
                    <?php
                    echo $this->Form->input('q', [
                        'label' => false,
                        'class' => 'form-control input-sm pull-right',
                        'placeholder' => __('Search'),
                        'value' => h($this->request->getQuery('q'))
                    ]); ?>

                    <?php echo $this->Form->input('segment_id', ['label' => false, 'options' => $segments, 'class' => 'input-sm', 'empty' => __('Segments'), 'value' => $this->request->getQuery('segment_id')]); ?>

                    <?php echo $this->Form->input('contact_type', ['options' => $contactTypesOptions, 'empty' => __('Contact Type'), 'label' => false, 'class' => 'input-sm', 'value' => $this->request->getQuery('contact_type')]); ?>

                    <?php echo $this->Form->input('state_id', ['options' => $states, 'empty' => __('Address State'), 'label' => false, 'class' => 'input-sm', 'value' => $this->request->getQuery('state_id')]); ?>
                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                <?= $this->Form->end(); ?>
            </div>
        </div>
        <div class="box-body">
        <?php if ($contacts->count() > 0): ?>
            <div class="box-group" id="accordion">
                <?php foreach ($contacts as $contact): ?>
                <div class="panel box box-default">
                    <div class="box-header with-border">
                        <h4 class="box-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= h($contact->id) ?>" aria-expanded="true" class="contact-name">
                                <strong><?= h($contact->name) ?></strong>
                                <span class="text-muted">
                                <?php if ($contact->telephones): ?>
                                    <?= $contact->telephones ?>
                                <?php endif; ?>
                                </span>
                            </a>
                        </h4>
                        <?php if($current_user['role'] == 'admin'): ?>
                        <div class="box-tools">
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $contact->id]) ?>
                            |
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $contact->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contact->id)]) ?>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div id="collapse<?= h($contact->id) ?>" class="panel-collapse collapse" aria-expanded="true">
                        <div class="box-body box-contact">
                            <ul class="list-unstyled list-contact-info">
                            <?php if ($contact->contact_type): ?>
                            <li>
                                <strong><?= __('Contact Type') ?>:</strong>
                                <?= $contact->contact_type ?>
                            </li>
                            <?php endif; ?>

                            <?php if ($contact->has('segment')): ?>
                            <li>
                                <strong><?= __('Segment') ?>:</strong>
                                <?= $contact->segment->name ?>
                            </li>
                            <?php endif; ?>

                            <?php if ($contact->contact_name): ?>
                            <li>
                                <strong><?= __('Contact') ?>:</strong>
                                <?= $contact->contact_name ?>
                            </li>
                            <?php endif; ?>

                            <?php if ($contact->department): ?>
                            <li>
                                <strong><?= __('Department') ?>:</strong>
                                <?= $contact->department ?>
                            </li>
                            <?php endif; ?>

                            <?php if ($contact->telephones): ?>
                            <li>
                                <strong><?= __('Telephone') ?>:</strong>
                                <?= $contact->telephones ?>
                            </li>
                            <?php endif; ?>

                            <?php if ($contact->fax): ?>
                            <li>
                                <strong><?= __('Fax') ?>:</strong>
                                <?= $contact->fax ?>
                            </li>
                            <?php endif; ?>

                            <?php if ($contact->mobile): ?>
                            <li>
                                <strong><?= __('Mobile') ?>:</strong>
                                <?= $contact->mobile ?>
                            </li>
                            <?php endif; ?>

                            <?php if ($contact->email): ?>
                            <li>
                                <strong><?= __('E-mail') ?>:</strong>
                                <?= $contact->email ?>
                            </li>
                            <?php endif; ?>

                            <?php if ($contact->skype): ?>
                            <li>
                                <strong><?= __('Skype') ?>:</strong>
                                <?= $contact->skype ?>
                            </li>
                            <?php endif; ?>

                            <?php if ($contact->site): ?>
                            <li>
                                <strong><?= __('Site') ?>:</strong>
                                <?= $contact->site ?>
                            </li>
                            <?php endif; ?>

                            <?php if ($contact->born_date): ?>
                            <li>
                                <strong><?= __('Born date') ?>:</strong>
                                <?= $contact->born_date ?>
                            </li>
                            <?php endif; ?>
                            </ul>

                            <ul class="list-unstyled list-contact-info">
                                <li><strong><i class="fa fa-map"></i> <?= __('Address') ?></strong><br /></li>
                                <?php if ($contact->address): ?>
                                <li><?= __('Address') ?>: <?= h($contact->address) ?></li>
                                <?php endif;?>

                                <?php if ($contact->address_number): ?>
                                <li><?= __('Address Number') ?>: <?= h($contact->address_number) ?></li>
                                <?php endif;?>

                                <?php if ($contact->address_complement): ?>
                                <li><?= __('Address Complement') ?>: <?= h($contact->address_complement) ?></li>
                                <?php endif;?>

                                <?php if ($contact->address_neighborhood): ?>
                                <li><?= __('Address Neighborhood') ?>: <?= h($contact->address_neighborhood) ?></li>
                                <?php endif;?>

                                <?php /*if ($contact->address_city): ?>
                                <li><?= __('Address City') ?>: <?= h($contact->address_city) ?></li>
                                <?php endif;?>

                                <?php if ($contact->address_state): ?>
                                <li><?= __('Address State') ?>: <?= h($contact->address_state) ?></li>
                                <?php endif;*/ ?>

                                <?php if ($contact->has('city')): ?>
                                <li>
                                    <?= __('Address City') ?>:
                                    <?= $contact->city->name ?>
                                </li>
                                <?php endif; ?>

                                <?php if ($contact->has('state')): ?>
                                <li>
                                    <?= __('Address State') ?>:
                                    <?= $contact->state->name ?>
                                </li>
                                <?php endif; ?>

                                <?php if ($contact->address_zipcode): ?>
                                <li><?= __('Address Zipcode') ?>: <?= h($contact->address_zipcode) ?></li>
                                <?php endif;?>
                            </ul>

                            <?php if ($contact->note): ?>
                            <p>
                                <strong><i class="fa fa-sticky-note"></i> <?= __('Note') ?>:</strong><br />
                                <?php echo h($contact->note); ?>
                            </p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>

            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <li><a href="#"><?= $this->Paginator->counter(__('{{count}} records. Page {{page}} of {{pages}}')); ?></a></li>
                </ul>
            </div>
        <?php else: ?>
            <p><?= __('No records found') ?></p>
        <?php endif; ?>
        </div>
    </div>
</section>
