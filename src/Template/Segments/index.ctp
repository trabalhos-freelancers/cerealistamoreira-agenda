<section class="content-header">
    <h1><i class="fa fa-list"></i> <?= __('Segments') ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-dashboard"></i> <?= __('Dashboard') ?></a></li>
        <li class="active"><?= __('Segments') ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <?= $this->Html->link(__('New Segment'), ['action' => 'add'], ['class' => 'btn btn-sm btn-success']) ?>
            <div class="box-tools">
                <?= $this->Form->create(null, [
                    'url' => '/segments',
                    'type' => 'get'
                ]); ?>
                <div class="input-group input-group-sm" style="width: 150px;">
                    <?php
                    echo $this->Form->input('q', [
                        'label' => false,
                        'class' => 'form-control input-sm pull-right',
                        'placeholder' => __('Search'),
                        'value' => h($this->request->getQuery('q'))
                    ]); ?>
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
        <div class="box-body">
        <?php if ($segments->count() > 0): ?>
            <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <?php if(false): ?>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <?php endif; ?>
                        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                        <?php if(false): ?>
                        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                        <?php endif; ?>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($segments as $segment): ?>
                    <tr>
                        <?php if(false): ?>
                        <td><?= $this->Number->format($segment->id) ?></td>
                        <?php endif; ?>
                        <td><?= h($segment->name) ?></td>
                        <?php if(false): ?>
                        <td><?= h($segment->created) ?></td>
                        <td><?= h($segment->modified) ?></td>
                        <?php endif; ?>
                        <td class="actions">
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $segment->id]) ?>
                            |
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $segment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $segment->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            </div>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <li><a href="#"><?= $this->Paginator->counter(__('{{count}} records. Page {{page}} of {{pages}}')); ?></a></li>
                </ul>
            </div>
        <?php else: ?>
            <p><?= __('No records found') ?></p>
        <?php endif; ?>
        </div>
    </div>
</section>
