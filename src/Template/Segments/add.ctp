<section class="content-header">
    <h1><i class="fa fa-list"></i> <?= __('Segments') ?><small><?= __('Add') ?></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-dashboard"></i> <?= __('Dashboard') ?></a></li>
        <li><?= $this->Html->link(__('Segments'), ['action' => 'index']) ?></li>
        <li class="active"><?= __('Add') ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default with-border">
        <div class="box-body">
            <?= $this->Form->create($segment) ?>
                <?php
                    echo $this->Form->input('name');
                ?>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
        <div class="box-footer">
            <?= $this->Html->link(__('Back list'), ['action' => 'index']) ?>
        </div>
    </div>
</section>
