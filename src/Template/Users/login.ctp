<!-- File: src/Template/Users/login.ctp -->
<div class="users form">
  <?= $this->Flash->render('auth') ?>
  <?= $this->Form->create() ?>
  <?= $this->Form->input('username') ?>
  <?= $this->Form->input('password') ?>
  <?= $this->Form->button(__('Entrar no sistema'), ['class' => 'btn btn-success btn-block']); ?>
  <?= $this->Form->end() ?>
</div>
