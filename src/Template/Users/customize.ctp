<section class="content-header">
    <h1><i class="fa fa-gears"></i> <?= __('Customize') ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-dashboard"></i> <?= __('Dashboard') ?></a></li>
        <li class="active"><?= __('Customize') ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default with-border">
        <div class="box-body">
            <?= $this->Form->create($user) ?>
            <div class="row">
                <div class="col-md-6">
                <?php
                    echo $this->Form->input('theme', ['options' => [
                        'skin-red-light' => __('Topo vermelho e sidebar claro'),
                        'skin-red' => __('Topo vermelho e sidebar escuro'),
                        'skin-yellow-light' => __('Topo amarelo e sidebar claro'),
                        'skin-yellow' => __('Topo amarelo e sidebar escuro'),
                        'skin-black-light' => __('Topo branco e sidebar claro'),
                        'skin-black' => __('Topo branco e sidebar escuro'),
                        'skin-blue-light' => __('Topo azul e sidebar claro'),
                        'skin-blue' => __('Topo azul e sidebar escuro'),
                        'skin-purple-light' => __('Topo roxo e sidebar claro'),
                        'skin-purple' => __('Topo roxo e sidebar escuro'),
                        'skin-green-light' => __('Topo verde e sidebar claro'),
                        'skin-green' => __('Topo verde e sidebar escuro')
                    ]]);

                    echo $this->Form->input('font_size', ['options' => [
                        '12' => '12 px',
                        '14' => '14 px',
                        '16' => '16 px',
                        '18' => '18 px',
                        '20' => '20 px',
                        '22' => '22 px',
                        '24' => '24 px',
                    ]]);
                ?>
                </div>
            </div>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</section>
