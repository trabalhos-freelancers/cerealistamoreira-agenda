<section class="content-header">
    <h1><i class="fa fa-user"></i> <?= __('Users') ?><small><?= __('Add') ?></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-dashboard"></i> <?= __('Dashboard') ?></a></li>
        <li><?= $this->Html->link(__('Users'), ['action' => 'index']) ?></li>
        <li class="active"><?= __('Add') ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default with-border">
        <div class="box-body">
            <?= $this->Form->create($user) ?>
            <div class="row">
                <div class="col-md-6">
                <?php
                    echo $this->Form->input('name');
                    echo $this->Form->input('email');
                    echo $this->Form->input('username');
                    echo $this->Form->input('password');
                    echo $this->Form->input('role', ['options' => [
                        'admin' => __('admin'),
                        'operator' => __('operator')
                    ]]);
                ?>
                </div>
            </div>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
        <div class="box-footer">
            <?= $this->Html->link(__('Back list'), ['action' => 'index']) ?>
        </div>
    </div>
</section>
