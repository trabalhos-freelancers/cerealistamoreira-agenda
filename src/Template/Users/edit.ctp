<section class="content-header">
    <h1><i class="fa fa-user"></i> <?= __('Users') ?><small><?= __('Edit') ?></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-dashboard"></i> <?= __('Dashboard') ?></a></li>
        <li><?= $this->Html->link(__('Users'), ['action' => 'index']) ?></li>
        <li class="active"><?= __('Edit') ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default with-border">
        <div class="box-body">
            <?= $this->Form->create($user) ?>
                <?php
                    echo $this->Form->input('name');
                    echo $this->Form->input('email');
                    echo $this->Form->input('username', ['disabled' => true]);
                    echo $this->Form->input('password');
                    echo $this->Form->input('role', ['options' => [
                        'admin' => __('admin'),
                        'operator' => __('operator')
                    ]]);
                ?>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
        <div class="box-footer">
            <ul class="list-inline">
                <li>
                <?= $this->Form->postLink(
                        __('Delete'),
                        ['action' => 'delete', $user->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
                    )
                ?>
                </li>
                <li><?= $this->Html->link(__('Back list'), ['action' => 'index']) ?></li>
            </ul>
        </div>
    </div>
</section>
