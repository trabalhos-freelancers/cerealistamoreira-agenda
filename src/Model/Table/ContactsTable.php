<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;

/**
 * Contacts Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Segments
 *
 * @method \App\Model\Entity\Contact get($primaryKey, $options = [])
 * @method \App\Model\Entity\Contact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Contact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Contact|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Contact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Contact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Contact findOrCreate($search, callable $callback = null)
 */
class ContactsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('contacts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Segments', [
            'foreignKey' => 'segment_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('States', [
            'foreignKey' => 'state_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('email')
            ->email('email');

        $validator
            ->requirePresence('segment_id', 'create')
            ->notEmpty('segment_id');

        $validator
            ->requirePresence('contact_type', 'create')
            ->notEmpty('contact_type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['segment_id'], 'Segments'));

        return $rules;
    }

    public function findBirthdays(Query $query)
    {
        $time = Time::now();
        return $query->where(['DATE_FORMAT(Contacts.born_date, \'%m-%d\') =' => $time->i18nFormat('MM-dd')]);
    }

    public function findBirthdaysOfMonth(Query $query)
    {
        $time = Time::now();
        $query->where(['DATE_FORMAT(Contacts.born_date, \'%d\') >=' => $time->i18nFormat('dd')]);
        $query->where(['DATE_FORMAT(Contacts.born_date, \'%m\') =' => $time->i18nFormat('MM')]);
        $query->order(['DATE_FORMAT(Contacts.born_date, \'%m-%d\')']);
        return $query;
    }

    public function findByKeyword(Query $query, array $options)
    {
        $keyword = $options['keyword'];

        if ($keyword != '') {
            $keyword = str_replace(' ', '%', $keyword);
            $query->where(function($exp, $q) use($keyword) {
                $concat = $q->func()->concat([
                    'Contacts.name' => 'literal',
                    'IFNULL(Contacts.contact_name, \'\')' => 'literal',
                    'IFNULL(Contacts.contact_type, \'\')' => 'literal',
                    'IFNULL(Contacts.address_city, \'\')' => 'literal',
                    'IFNULL(Contacts.address_state, \'\')' => 'literal',
                    'IFNULL(Contacts.note, \'\')' => 'literal',
                    'IFNULL(Segments.name, \'\')' => 'literal'
                ]);
                return $exp->like($concat, "%$keyword%");
            });
        }

        return $query;
    }
}
