<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Segments Model
 *
 * @method \App\Model\Entity\Segment get($primaryKey, $options = [])
 * @method \App\Model\Entity\Segment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Segment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Segment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Segment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Segment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Segment findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SegmentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('segments');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Contacts', [
            'foreignKey' => 'segment_id',
            'dependent' => true
        ]);

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }

    public function top10()
    {
        $query = $this->find()
        ->innerJoinWith('Contacts')
        ->group(['Segments.id'])
        ->order(['total_contacts DESC'])
        ->limit(10);

        $count = $query->func()->count('Contacts.id');
        $query->select([
            'Segments.id',
            'Segments.name',
            'Segments.created',
            'Segments.modified',
            'total_contacts' => $count
        ]);

        return $query;
    }
}
