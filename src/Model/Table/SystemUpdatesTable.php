<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SystemUpdates Model
 *
 * @method \App\Model\Entity\SystemUpdate get($primaryKey, $options = [])
 * @method \App\Model\Entity\SystemUpdate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SystemUpdate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SystemUpdate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SystemUpdate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SystemUpdate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SystemUpdate findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SystemUpdatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('system_updates');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('update_date')
            ->requirePresence('update_date', 'create')
            ->notEmpty('update_date');

        $validator
            ->requirePresence('update_type', 'create')
            ->notEmpty('update_type');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }

    public static function updateTypeOptions()
    {
        return [
            'bug' => 'BUG - Correção de erro do sistema',
            'new_feature' => 'Nova funcionalidade'
        ];
    }

    public function totalDontRead($last_system_update)
    {
        $query = $this->find();
        $query->where(['SystemUpdates.id >' => $last_system_update]);
        return $query->count(['SystemUpdates.id']);
    }

    public function findLastRecord()
    {
        return $this->find()->order(['SystemUpdates.id DESC'])->limit(1)->first();
    }
}
