<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;

/**
 * Reminders Model
 *
 * @method \App\Model\Entity\Reminder get($primaryKey, $options = [])
 * @method \App\Model\Entity\Reminder newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Reminder[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Reminder|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Reminder patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Reminder[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Reminder findOrCreate($search, callable $callback = null)
 */
class RemindersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('reminders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('due_date')
            ->requirePresence('due_date', 'create')
            ->notEmpty('due_date');

        $validator
            ->requirePresence('content', 'create')
            ->notEmpty('content');

        return $validator;
    }

    public function findToday(Query $query)
    {
        $time = Time::now();
        return $query->where(['due_date' => $time->i18nFormat('yyyy-MM-dd')]);
    }

    public function findNextSevenDays(Query $query)
    {
        $today = Time::now();
        $nextSevenDays = Time::now();
        $nextSevenDays->addDays(7);

        return $query->where(function($exp) use ($today, $nextSevenDays) {
            return $exp->between(
                'Reminders.due_date',
                $today->i18nFormat('yyyy-MM-dd'),
                $nextSevenDays->i18nFormat('yyyy-MM-dd')
            );
        }) 
        ->andWhere([
            'OR' => [
                'visibility' => 'PUBLIC',
                'author' => $_SESSION['Auth']['User']['name'],
                'user_id' => $_SESSION['Auth']['User']['id']
            ]
        ])
        ->order(['Reminders.due_date']);
    }

    public function totalOpen( $user )
    {
        $query = $this->find();
        $query
            ->where(['Reminders.due_date >=' => Time::now()->i18nFormat('yyyy-MM-dd')])
            ->andWhere([
                'OR' => [
                    'visibility' => 'PUBLIC',
                    'author' => $user['name'],
                    'user_id' => $user['id']
            ]
        ]);
        return $query->count(['Reminders.id']);
    }

    public function findByKeyword(Query $query, array $options)
    {
        $keyword = $options['keyword'];

        if ($keyword != '') {
            $keyword = str_replace(' ', '%', $keyword);
            $query->where(function($exp, $q) use($keyword) {
                $concat = $q->func()->concat([
                    'DATE_FORMAT(Reminders.due_date, \'%d/%m/%Y\')' => 'literal',
                    'Reminders.content' => 'literal'
                ]);
                return $exp->like($concat, "%$keyword%");
            });
        }

        return $query;
    }
}
