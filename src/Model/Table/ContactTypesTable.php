<?php
namespace App\Model\Table;

class ContactTypesTable
{
    public static function collectionSelect()
    {
        return [
            'cliente' => 'Cliente',
            'fornecedor' => 'Fornecedor',
            'cliente e fornecedor' => 'Cliente e Fornecedor',
            'outro' => 'Outro'
        ];
    }
}
