<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contact Entity
 *
 * @property int $id
 * @property string $name
 * @property string $contact_name
 * @property string $department
 * @property int $segment_id
 * @property string $contact_type
 * @property string $address
 * @property string $address_number
 * @property string $address_complement
 * @property string $address_neighborhood
 * @property string $address_city
 * @property string $address_state
 * @property string $address_zipcode
 * @property string $telephones
 * @property string $fax
 * @property string $mobile
 * @property string $email
 * @property string $skype
 * @property string $site
 * @property \Cake\I18n\Time $born_date
 * @property string $note
 *
 * @property \App\Model\Entity\Segment $segment
 */
class Contact extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
