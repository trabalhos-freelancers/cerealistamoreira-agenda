<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SystemUpdates Controller
 *
 * @property \App\Model\Table\SystemUpdatesTable $SystemUpdates
 */
class SystemUpdatesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // Atualiza a última atualização vista.
        $current_user = $this->Auth->user();
        if ($this->SystemUpdates->totalDontRead($current_user['last_system_update']) > 0) {
            $lastSystemUpdate = $this->SystemUpdates->find('lastRecord');

            if ($lastSystemUpdate) {
                $this->loadModel('Users');
                $user = $this->Users->get($current_user['id']);
                $user->last_system_update = $lastSystemUpdate->id;
                $this->Users->save($user);
                $this->Auth->setUser($user);
            }
        }
        // Atualiza a última atualização vista.

        $this->paginate = [
            'order' => ['SystemUpdates.update_date DESC, SystemUpdates.id DESC']
        ];

        $systemUpdates = $this->paginate($this->SystemUpdates);

        $this->set(compact('systemUpdates'));
        $this->set('_serialize', ['systemUpdates']);
    }

    /**
     * View method
     *
     * @param string|null $id System Update id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $systemUpdate = $this->SystemUpdates->get($id, [
            'contain' => []
        ]);

        $this->set('systemUpdate', $systemUpdate);
        $this->set('_serialize', ['systemUpdate']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $systemUpdate = $this->SystemUpdates->newEntity();
        $systemUpdate->update_date = date('Y-m-d');
        if ($this->request->is('post')) {
            $systemUpdate = $this->SystemUpdates->patchEntity($systemUpdate, $this->request->getData());
            if ($this->SystemUpdates->save($systemUpdate)) {
                $this->Flash->success(__('The system update has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The system update could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('systemUpdate'));
        $this->set('_serialize', ['systemUpdate']);
    }

    /**
     * Edit method
     *
     * @param string|null $id System Update id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $systemUpdate = $this->SystemUpdates->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $systemUpdate = $this->SystemUpdates->patchEntity($systemUpdate, $this->request->getData());
            if ($this->SystemUpdates->save($systemUpdate)) {
                $this->Flash->success(__('The system update has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The system update could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('systemUpdate'));
        $this->set('_serialize', ['systemUpdate']);
    }

    /**
     * Delete method
     *
     * @param string|null $id System Update id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $systemUpdate = $this->SystemUpdates->get($id);
        if ($this->SystemUpdates->delete($systemUpdate)) {
            $this->Flash->success(__('The system update has been deleted.'));
        } else {
            $this->Flash->error(__('The system update could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
