<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Home Controller
 *
 * @property \App\Model\Table\HomeTable $Home
 */
class HomeController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $models = ['Contacts', 'Reminders', 'Segments'];
        foreach ($models as $model) {
            $this->loadModel($model);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->set('totalContacts', $this->Contacts->find()->count());
        $this->set('totalSegments', $this->Segments->find()->count());
        $this->set('top10Segments', $this->Segments->top10());
        $this->set('reminders', $this->Reminders->find('nextSevenDays'));
        $this->set('reminderTotalOpen', $this->Reminders->totalOpen( $this->Auth->user() ));
        $this->set('birthdays', $this->Contacts->find('BirthdaysOfMonth'));
    }
}
