<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * Reminders Controller
 *
 * @property \App\Model\Table\RemindersTable $Reminders
 */
class RemindersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'order' => ['due_date']
        ];

        $query = $this->Reminders->find();
        if ($this->request->getQuery('q') != '') {
            $query->find('byKeyword', [
                'keyword' => h($this->request->getQuery('q'))
            ]);
        }
        $query->where(
            ['Reminders.due_date >=' => Time::now()->i18nFormat('yyyy-MM-dd')]
        )
        ->andWhere([
            'OR' => [
                'visibility' => 'PUBLIC',
                'author' => $this->Auth->user()['name'],
                'user_id' => $this->Auth->user()['id']
            ]
        ]);

        $reminders = $this->paginate($query);

        $this->set(compact('reminders'));
        $this->set('_serialize', ['reminders']);
    }

    /**
     * View method
     *
     * @param string|null $id Reminder id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $reminder = $this->Reminders->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('reminder', $reminder);
        $this->set('_serialize', ['reminder']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $current_user = $this->Auth->user();
        $reminder = $this->Reminders->newEntity();
        $reminder->due_date = date('Y-m-d');

        if ($this->request->is('post')) {
            $reminder = $this->Reminders->patchEntity($reminder, $this->request->getData());
            if ($this->Reminders->save($reminder)) {
                $this->Flash->success(__('The reminder has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The reminder could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('reminder'));
        $this->set(compact('current_user'));

        $this->loadModel('Users');
        $this->set('users', $this->Users->find('all'));
        $this->set('_serialize', ['reminder']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Reminder id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $current_user = $this->Auth->user();
        $reminder = $this->Reminders->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reminder = $this->Reminders->patchEntity($reminder, $this->request->getData());
            if ($this->Reminders->save($reminder)) {
                $this->Flash->success(__('The reminder has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The reminder could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('reminder'));
        $this->set(compact('current_user'));

        $this->loadModel('Users');
        $this->set('users', $this->Users->find('all'));
        $this->set('_serialize', ['reminder']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Reminder id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $reminder = $this->Reminders->get($id);
        if ($this->Reminders->delete($reminder)) {
            $this->Flash->success(__('The reminder has been deleted.'));
        } else {
            $this->Flash->error(__('The reminder could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
