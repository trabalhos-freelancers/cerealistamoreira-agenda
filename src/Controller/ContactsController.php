<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\StatesTable;
use App\Model\Table\CitiesTable;
use App\Model\Table\ContactTypesTable;

/**
 * Contacts Controller
 *
 * @property \App\Model\Table\ContactsTable $Contacts
 */
class ContactsController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('States');
        $this->loadModel('Cities');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'Segments', 'States', 'Cities'
            ]
        ];

        $query = $this->Contacts->find();
        if ($this->request->getQuery('q') != '') {
            $query->find('byKeyword', [
                'keyword' => h($this->request->getQuery('q'))
            ]);
        }

        if ($this->request->getQuery('segment_id') != '') {
            $query->where(['Contacts.segment_id' => (int) $this->request->getQuery('segment_id')]);
        }

        if ($this->request->getQuery('contact_type') != '') {
            $query->where(['Contacts.contact_type' => h($this->request->getQuery('contact_type'))]);
        }

        if ($this->request->getQuery('state_id') != '') {
            $query->where(['Contacts.state_id' => (int) $this->request->getQuery('state_id')]);
        }

        $query->order(['Contacts.name']);

        $contacts = $this->paginate($query);

        $this->set(compact('contacts'));
        $this->set('_serialize', ['contacts']);

        $segments = $this->Contacts->Segments->find('list', ['order' => ['name']]);
        $this->set('segments', $segments);
        $this->set('contactTypesOptions', ContactTypesTable::collectionSelect());
        //$this->set('stateOptions', StatesTable::collectionSelect());
        $this->set('states', $this->States->find('list', [
            'keyField' => 'id',
            'valueField' => ['acronym'],
            'order' => ['name']
        ]));
    }

    /**
     * View method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contact = $this->Contacts->get($id, [
            'contain' => ['Segments']
        ]);

        $this->set('contact', $contact);
        $this->set('_serialize', ['contact']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contact = $this->Contacts->newEntity();
        if ($this->request->is('post')) {
            $contact = $this->Contacts->patchEntity($contact, $this->request->getData());
            if ($this->Contacts->save($contact)) {
                $this->Flash->success(__('The contact has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The contact could not be saved. Please, try again.'));
            }
        }
        $this->set('_serialize', ['contact']);
        $segments = $this->Contacts->Segments->find('list', ['order' => ['name']]);
        $this->set(compact('contact', 'segments'));
        $this->set('states', $this->States->find('list', [
            'keyField' => 'id',
            'valueField' => ['acronym'],
            'order' => ['name']
        ]));
        //$this->set('stateOptions', StatesTable::collectionSelect());
        $this->set('contactTypesOptions', ContactTypesTable::collectionSelect());
    }

    /**
     * Edit method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contact = $this->Contacts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contact = $this->Contacts->patchEntity($contact, $this->request->getData());
            if ($this->Contacts->save($contact)) {
                $this->Flash->success(__('The contact has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The contact could not be saved. Please, try again.'));
            }
        }
        $segments = $this->Contacts->Segments->find('list', ['order' => ['name']]);
        $this->set(compact('contact', 'segments'));
        $this->set('_serialize', ['contact']);
        $this->set('states', $this->States->find('list', [
            'keyField' => 'id',
            'valueField' => ['acronym'],
            'order' => ['name']
        ]));
        $this->set('cities', $this->Cities->find('list')->find('state', [
            'state' => $contact->state_id
        ])->order(['Cities.name']));
        //$this->set('stateOptions', StatesTable::collectionSelect());
        $this->set('contactTypesOptions', ContactTypesTable::collectionSelect());
    }

    /**
     * Delete method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contact = $this->Contacts->get($id);
        if ($this->Contacts->delete($contact)) {
            $this->Flash->success(__('The contact has been deleted.'));
        } else {
            $this->Flash->error(__('The contact could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
